<?php

namespace soc\yiiuser\User\Query;

use yii\db\ActiveQuery;

class ProfileQuery extends ActiveQuery
{
    /**
     * Search by user id
     * @param  mixed       $user_id
     * @return ActiveQuery
     */
    public function whereUserId($user_id)
    {
        return $this->andWhere(['user_id' => $user_id]);
    }
}
