<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Controller;

use soc\yiiuser\User\Event\FormEvent;
use soc\yiiuser\User\Event\ResetPasswordEvent;
use soc\yiiuser\User\Factory\MailFactory;
use soc\yiiuser\User\Form\RecoveryForm;
use soc\yiiuser\User\Model\Token;
use soc\yiiuser\User\Module;
use soc\yiiuser\User\Query\TokenQuery;
use soc\yiiuser\User\Query\UserQuery;
use soc\yiiuser\User\Service\PasswordRecoveryService;
use soc\yiiuser\User\Service\ResetPasswordService;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use soc\yiiuser\User\Traits\ModuleAwareTrait;
use soc\yiiuser\User\Validator\AjaxRequestModelValidator;
use Yii;
use yii\base\InvalidConfigException;
use yii\base\InvalidParamException;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\NotFoundHttpException;

class RecoveryController extends Controller
{
    use ContainerAwareTrait;
    use ModuleAwareTrait;

    protected $userQuery;
    protected $tokenQuery;

    /**
     * RecoveryController constructor.
     *
     * @param string     $id
     * @param Module     $module
     * @param UserQuery  $userQuery
     * @param TokenQuery $tokenQuery
     * @param array      $config
     */
    public function __construct($id, Module $module, UserQuery $userQuery, TokenQuery $tokenQuery, array $config = [])
    {
        $this->userQuery = $userQuery;
        $this->tokenQuery = $tokenQuery;
        parent::__construct($id, $module, $config);
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::class,
                'rules' => [
                    [
                        'allow' => true,
                        'actions' => ['request', 'reset'],
                        'roles' => ['?'],
                    ],
                ],
            ],
        ];
    }

    /**
     * Displays / handles user password recovery request.
     *
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     * @throws InvalidParamException
     * @return string
     *
     */
    public function actionRequest()
    {
        if (!$this->module->allowPasswordRecovery) {
            throw new NotFoundHttpException();
        }

        /** @var RecoveryForm $form */
        $form = $this->make(RecoveryForm::class, [], ['scenario' => RecoveryForm::SCENARIO_REQUEST]);

        $event = $this->make(FormEvent::class, [$form]);

        $this->make(AjaxRequestModelValidator::class, [$form])->validate();

        if ($form->load(Yii::$app->request->post()) && $form->validate()) {
            $this->trigger(FormEvent::EVENT_BEFORE_REQUEST, $event);

            $mailService = MailFactory::makeRecoveryMailerService($form->email);

            if ($this->make(PasswordRecoveryService::class, [$form->email, $mailService])->run()) {
                $this->trigger(FormEvent::EVENT_AFTER_REQUEST, $event);
            }

            return $this->render(
                '/shared/message',
                [
                    'title' => Yii::t('app', 'Recovery message sent'),
                    'module' => $this->module,
                ]
            );
        }

        return $this->render('request', ['model' => $form]);
    }

    /**
     * Displays / handles user password reset.
     *
     * @param $id
     * @param $code
     *
     * @throws NotFoundHttpException
     * @throws InvalidConfigException
     * @throws InvalidParamException
     * @return string
     *
     */
    public function actionReset($id, $code)
    {
        if (!$this->module->allowPasswordRecovery && !$this->module->allowAdminPasswordRecovery) {
            throw new NotFoundHttpException();
        }
        /** @var Token $token */
        $token = $this->tokenQuery->whereUserId($id)->whereCode($code)->whereIsRecoveryType()->one();
        /** @var ResetPasswordEvent $event */
        $event = $this->make(ResetPasswordEvent::class, [$token]);

        $this->trigger(ResetPasswordEvent::EVENT_BEFORE_TOKEN_VALIDATE, $event);

        if ($token === null || $token->getIsExpired() || $token->user === null) {
            Yii::$app->session->setFlash(
                'danger',
                Yii::t('app', 'Recovery link is invalid or expired. Please try requesting a new one.')
            );

            return $this->render(
                '/shared/message',
                [
                    'title' => Yii::t('app', 'Invalid or expired link'),
                    'module' => $this->module,
                ]
            );
        }

        /** @var RecoveryForm $form */
        $form = $this->make(RecoveryForm::class, [], ['scenario' => RecoveryForm::SCENARIO_RESET]);
        $event = $event->updateForm($form);

        $this->make(AjaxRequestModelValidator::class, [$form])->validate();

        if ($form->load(Yii::$app->getRequest()->post())) {
            if ($this->make(ResetPasswordService::class, [$form->password, $token->user])->run()) {
                $this->trigger(ResetPasswordEvent::EVENT_AFTER_RESET, $event);

                Yii::$app->session->setFlash('success', Yii::t('app', 'Password has been changed'));

                return $this->render(
                    '/shared/message',
                    [
                        'title' => Yii::t('app', 'Password has been changed'),
                        'module' => $this->module,
                    ]
                );
            }
        }

        return $this->render('reset', ['model' => $form]);
    }
}
