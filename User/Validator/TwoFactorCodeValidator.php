<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Validator;

use soc\yiiuser\TwoFA\Exception\InvalidSecretKeyException;
use soc\yiiuser\TwoFA\Manager;
use soc\yiiuser\User\Contracts\ValidatorInterface;
use soc\yiiuser\User\Model\User;
use soc\yiiuser\User\Service\TwoFactorQrCodeUriGeneratorService;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use Yii;

class TwoFactorCodeValidator implements ValidatorInterface
{
    use ContainerAwareTrait;

    protected $user;
    protected $code;
    protected $cycles;

    /**
     * TwoFactorCodeValidator constructor.
     *
     * @param User $user
     * @param $code
     * @param int $cycles
     */
    public function __construct(User $user, $code, $cycles = 0)
    {
        $this->user = $user;
        $this->code = $code;
        $this->cycles = $cycles;
    }

    /**
     * @throws InvalidSecretKeyException
     * @return bool|int
     *
     */
    public function validate()
    {
        $manager = new Manager();
        return $manager->setCycles($this->cycles)->verify($this->code, $this->user->auth_tf_key);
    }

    /**
     * @return bool
     *
     */
    public function isValidationCodeToBeSent()
    {
        return false;
    }

    /**
    * @return string
    *
    */
    public function getSuccessMessage()
    {
        return Yii::t('app', 'Two factor authentication successfully enabled.');
    }

    /**
     *
     * @param  mixed  $codeDurationTime
     * @return string
     */
    public function getUnsuccessMessage($codeDurationTime)
    {
        return Yii::t('app', 'Verification failed. Please, enter new code.');
    }

    /**
     *
     * @param  mixed  $codeDurationTime
     * @return string
     */
    public function getUnsuccessLoginMessage($codeDurationTime)
    {
        return Yii::t('app', 'Verification failed. Please, enter new code.');
    }

    /**
    * @return string
    *
    */
    public function generateCode()
    {
        return $this->make(TwoFactorQrCodeUriGeneratorService::class, [$this->user])->run();
    }
}
