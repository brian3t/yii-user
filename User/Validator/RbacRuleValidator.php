<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Validator;

use Exception;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use Yii;
use yii\rbac\Rule;
use yii\validators\Validator;

class RbacRuleValidator extends Validator
{
    use ContainerAwareTrait;

    protected function validateValue($value)
    {
        try {
            $rule = $this->make($value);

            if (!($rule instanceof Rule)) {
                return [Yii::t('app', 'Rule class must extend "yii\\rbac\\Rule".'), []];
            }
        } catch (Exception $e) {
            return [Yii::t('app', 'Authentication rule class {0} can not be instantiated', $value), []];
        }
    }
}
