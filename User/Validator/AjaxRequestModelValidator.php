<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Validator;

use soc\yiiuser\User\Contracts\ValidatorInterface;
use Yii;
use yii\base\Model;
//use yii\bootstrap\ActiveForm;
use yii\bootstrap5\ActiveForm;
use yii\web\Response;

class AjaxRequestModelValidator implements ValidatorInterface
{
    protected $model;

    public function __construct(Model $model)
    {
        $this->model = $model;
    }

    public function validate()
    {
        $request = Yii::$app->request;

        if ($request->getIsAjax() ){
            $load_result = $this->model->load($request->post());
            if ($load_result) return false;
            Yii::$app->response->format = Response::FORMAT_JSON;
            $result = ActiveForm::validate($this->model);//b3t
            Yii::$app->response->data = $result;
            Yii::$app->response->send();
            Yii::$app->end();
            return $result;
        }
        return false;
    }
}
