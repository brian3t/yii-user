<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Service;

use Exception;
use soc\yiiuser\User\Contracts\ServiceInterface;
use soc\yiiuser\User\Event\UserEvent;
use soc\yiiuser\User\Factory\TokenFactory;
use soc\yiiuser\User\Helper\SecurityHelper;
use soc\yiiuser\User\Model\User;
use soc\yiiuser\User\Traits\MailAwareTrait;
use soc\yiiuser\User\Traits\ModuleAwareTrait;
use Yii;
use yii\base\InvalidCallException;

class UserRegisterService implements ServiceInterface
{
    use ModuleAwareTrait;
    use MailAwareTrait;

    protected $model;
    protected $securityHelper;
    protected $mailService;

    public function __construct(User $model, MailService $mailService, SecurityHelper $securityHelper)
    {
        $this->model = $model;
        $this->mailService = $mailService;
        $this->securityHelper = $securityHelper;
    }

    public function run()
    {
        $model = $this->model;

        if ($model->getIsNewRecord() === false) {
            throw new InvalidCallException('Cannot register user from an existing one.');
        }

        $transaction = $model::getDb()->beginTransaction();

        try {
            $model->confirmed_at = $this->getModule()->enableEmailConfirmation ? null : time();
            $model->password = $this->getModule()->generatePasswords
                ? $this->securityHelper->generatePassword(8, $this->getModule()->minPasswordRequirements)
                : $model->password;

            $event = $this->make(UserEvent::class, [$model]);
            $model->trigger(UserEvent::EVENT_BEFORE_REGISTER, $event);

            if (!$model->save()) {
                $transaction->rollBack();
                return false;
            }

            if ($this->getModule()->enableEmailConfirmation) {
                $token = TokenFactory::makeConfirmationToken($model->id);
            }

            if (isset($token)) {
                $this->mailService->setViewParam('token', $token);
            }
            if (!$this->sendMail($model)) {
                Yii::$app->session->setFlash(
                    'warning',
                    Yii::t(
                        'app',
                        'Error sending registration message to "{email}". Please try again later.',
                        ['email' => $model->email]
                    )
                );
                $transaction->rollBack();
                return false;
            }
            $model->trigger(UserEvent::EVENT_AFTER_REGISTER, $event);

            $transaction->commit();

            return true;
        } catch (Exception $e) {
            $transaction->rollBack();
            Yii::error($e->getMessage(), 'app');

            return false;
        }
    }
}
