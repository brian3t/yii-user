<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Service;

use soc\yiiuser\User\Contracts\AuthClientInterface;
use soc\yiiuser\User\Contracts\ServiceInterface;
use soc\yiiuser\User\Controller\SecurityController;
use soc\yiiuser\User\Event\SocialNetworkAuthEvent;
use soc\yiiuser\User\Model\SocialNetworkAccount;
use soc\yiiuser\User\Model\User;
use soc\yiiuser\User\Query\SocialNetworkAccountQuery;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use Yii;

class SocialNetworkAccountConnectService implements ServiceInterface
{
    use ContainerAwareTrait;

    protected $controller;
    protected $client;
    protected $socialNetworkAccountQuery;

    /**
     * SocialNetworkAccountUserLinkService constructor.
     *
     * @param SecurityController        $controller
     * @param AuthClientInterface       $client
     * @param SocialNetworkAccountQuery $socialNetworkAccountQuery
     */
    public function __construct(
        SecurityController $controller,
        AuthClientInterface $client,
        SocialNetworkAccountQuery $socialNetworkAccountQuery
    ) {
        $this->controller = $controller;
        $this->client = $client;
        $this->socialNetworkAccountQuery = $socialNetworkAccountQuery;
    }

    public function run()
    {
        $account = $this->getSocialNetworkAccount();

        $event = $this->make(SocialNetworkAuthEvent::class, [$account, $this->client]);

        $this->controller->trigger(SocialNetworkAuthEvent::EVENT_BEFORE_CONNECT, $event);

        if ($account && $account->user === null) {
            /** @var User $user */
            $user = Yii::$app->user->identity;
            $account->link('user', $user);
            Yii::$app->session->setFlash('success', Yii::t('app', 'Your account has been connected'));
            $this->controller->trigger(SocialNetworkAuthEvent::EVENT_AFTER_CONNECT, $event);

            return true;
        }
        Yii::$app->session->setFlash(
            'danger',
            Yii::t('app', 'This account has already been connected to another user')
        );

        return false;
    }

    protected function getSocialNetworkAccount()
    {
        $account = $this->socialNetworkAccountQuery->whereClient($this->client)->one();

        if (null === $account) {
            $data = $this->client->getUserAttributes();

            $account = $this->make(
                SocialNetworkAccount::class,
                [],
                [
                    'provider' => $this->client->getId(),
                    'client_id' => $data['id'],
                    'data' => json_encode($data),
                ]
            );

            if ($account->save(false)) {
                return $account;
            }
        }

        return false;
    }
}
