<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */


use yii\db\Migration;

class m221205_010525_yiiuser_07_enable_password_expiration extends Migration
{
    public function safeUp()
    {
        $this->addColumn('{{%user}}', 'password_changed_at', $this->integer()->null());
    }

    public function safeDown()
    {
        $this->dropColumn('{{%user}}', 'password_changed_at');
    }
}
