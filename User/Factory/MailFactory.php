<?php

namespace soc\yiiuser\User\Factory;

use soc\yiiuser\User\Event\MailEvent;
use soc\yiiuser\User\Model\Token;
use soc\yiiuser\User\Model\User;
use soc\yiiuser\User\Module;
use soc\yiiuser\User\Service\MailService;
use Yii;
use yii\base\InvalidConfigException;

//$env_path = realpath('../../../../config/envv.php');
/* brian 5/25/24: What is this env_path??
$env_path = realpath('./config/envv.php');
if (!$env_path) $env_path = realpath('../../config/envv.php');
require_once $env_path;*/

class MailFactory
{
    /**
     * @param User $user
     * @param bool $showPassword
     *
     * @throws InvalidConfigException
     * @return MailService
     */
    public static function makeWelcomeMailerService(User $user, $showPassword = false)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('user');
        //b3t set up mailParams; or use default values here
        if (!$module->mailParams) $module->mailParams = [];
        $to = $user->email;
        $from = $module?->mailParams['fromEmail'] ?? EMAIL_FROM;
        $subject = $module?->mailParams['welcomeMailSubject'] ?? WELCOME_MAIL_SUBJ;
        $params = [
            'user' => $user,
            'token' => null,
            'module' => $module,
            'showPassword' => $showPassword,
        ];

        return static::makeMailerService(MailEvent::TYPE_WELCOME, $from, $to, $subject, 'welcome', $params);
    }

    /**
     * @param string $email
     * @param Token  $token
     *
     * @throws InvalidConfigException
     * @return MailService
     */
    public static function makeRecoveryMailerService($email, Token $token = null)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('user');

        $to = $email;
        $from = $module->mailParams['fromEmail'];
        $subject = $module->mailParams['recoveryMailSubject'];
        $params = [
            'user' => $token && $token->user ? $token->user : null,
            'token' => $token,
        ];

        return static::makeMailerService(MailEvent::TYPE_RECOVERY, $from, $to, $subject, 'recovery', $params);
    }

    /**
     * @param User       $user
     * @param Token|null $token
     *
     * @throws InvalidConfigException
     * @return MailService
     */
    public static function makeConfirmationMailerService(User $user, Token $token = null)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('user');

        $to = $user->email;
        $from = $module->mailParams['fromEmail'];
        $subject = $module->mailParams['confirmationMailSubject'];
        $params = [
            'user' => $token && $token->user ? $token->user : null,
            'token' => $token,
        ];

        return static::makeMailerService(MailEvent::TYPE_CONFIRM, $from, $to, $subject, 'confirmation', $params);
    }

    /**
     * @param User  $user
     * @param Token $token
     *
     * @throws InvalidConfigException
     * @return MailService
     */
    public static function makeReconfirmationMailerService(User $user, Token $token)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('user');

        $to = $token->type === Token::TYPE_CONFIRM_NEW_EMAIL
            ? $user->unconfirmed_email
            : $user->email;

        $from = $module->mailParams['fromEmail'];
        $subject = $module->mailParams['reconfirmationMailSubject'];
        $params = [
            'user' => $token && $token->user ? $token->user : null,
            'token' => $token,
        ];

        return static::makeMailerService(MailEvent::TYPE_RECONFIRM, $from, $to, $subject, 'reconfirmation', $params);
    }

    /**
     * @param User   $user
     * @param String $code
     *
     * @throws InvalidConfigException
     * @return MailService
     */
    public static function makeTwoFactorCodeMailerService(User $user, String $code)
    {
        /** @var Module $module */
        $module = Yii::$app->getModule('user');

        $to = $user->email;

        $from = $module->mailParams['fromEmail'];
        $subject = $module->mailParams['twoFactorMailSubject'];
        $params = [
            'code' => $code,
        ];

        return static::makeMailerService(MailEvent::TYPE_TWOFACTORCODE, $from, $to, $subject, 'twofactorcode', $params);
    }

    /**
     * Builds a MailerService.
     *
     * @param string                $type
     * @param string|array|\Closure $from
     * @param string                $to
     * @param string                $subject
     * @param string                $view
     * @param array                 $params
     *
     * @throws InvalidConfigException
     * @return MailService
     *
     */
    public static function makeMailerService($type, $from, $to, $subject, $view, $params = [])
    {
        if ($from instanceof \Closure) {
            $from = $from($type);
        }
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return Yii::$container->get(
            MailService::class,
            [$type, $from, $to, $subject, $view, $params, Yii::$app->getMailer()]
        );
    }
}
