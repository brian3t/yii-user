<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var \soc\yiiuser\User\Form\RegistrationForm $model
 * @var \soc\yiiuser\User\Model\User $user
 * @var \soc\yiiuser\User\Module $module
 */

$this->title = Yii::t('app', 'Sign up');
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="row content_center pos_center col-md-6">
  <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><?= Html::encode($this->title) ?></h3>
    </div>
    <br>
    <div class="panel-body">
      <?php $form = ActiveForm::begin(
        [
          'id' => $model->formName(),
          'enableAjaxValidation' => true,
          'enableClientValidation' => false,
        ]
      ); ?>

      <?= $form->field($model, 'email')->textInput(['autofocus' => true]) ?>

      <?= $form->field($model, 'username') ?>

      <?php if ($module->generatePasswords === false): ?>
        <?= $form->field($model, 'password')->passwordInput() ?>
      <?php endif ?>

      <?php if ($module && $module->enableGdprCompliance): ?>
        <?= $form->field($model, 'gdpr_consent')->checkbox(['value' => 1]) ?>
      <?php endif ?>

      <?= Html::submitButton(Yii::t('app', 'Sign up'), ['class' => 'btn btn-success btn-block']) ?>

      <?php ActiveForm::end(); ?>
    </div>
  </div>
  <p class="text-center">
    <?= Html::a(Yii::t('app', 'Already registered? Sign in!'), ['/user/security/login']) ?>
  </p>
</div>
