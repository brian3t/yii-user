<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use soc\yiiuser\User\Model\User;
use soc\yiiuser\User\Module as UserModule;
use yii\helpers\Html;
use yii\widgets\Menu;

/** @var User $user */
$user = Yii::$app->user->identity;
/** @var UserModule $module */
$module = Yii::$app->getModule('user');
$networksVisible = count(Yii::$app->authClientCollection->clients) > 0;

?>

<div class="panel panel-default">
    <div class="panel-heading">
        <h3 class="panel-title">
            <?= Html::img(
                $user->profile->getAvatarUrl(24),
                [
                    'class' => 'img-rounded',
                    'alt' => $user->username,
                ]
            ) ?>
            <?= $user->username ?>
        </h3>
    </div>
    <div class="panel-body">
        <?= Menu::widget(
            [
                'options' => [
                    'class' => 'nav nav-pills nav-stacked',
                ],
                'items' => [
                    ['label' => Yii::t('app', 'Profile'), 'url' => ['/user/settings/profile']],
                    ['label' => Yii::t('app', 'Account'), 'url' => ['/user/settings/account']],
                    [
                        'label' => Yii::t('app', 'Session history'),
                        'url' => ['/user/settings/session-history'],
                        'visible' => $module->enableSessionHistory,
                    ],
                    ['label' => Yii::t('app', 'Privacy'),
                        'url' => ['/user/settings/privacy'],
                        'visible' => ($this->module? $module->enableGdprCompliance: false)
                    ],
                    [
                        'label' => Yii::t('app', 'Networks'),
                        'url' => ['/user/settings/networks'],
                        'visible' => $networksVisible,
                    ],
                ],
            ]
        ) ?>
    </div>
</div>
