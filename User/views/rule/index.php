<?php

use yii\grid\ActionColumn;
use yii\grid\GridView;
use yii\helpers\Url;
use yii\rbac\Rule;

/**
 * @var \yii\data\ActiveDataProvider $dataProvider
 * @var \soc\yiiuser\User\Search\RuleSearch $searchModel
 * @var yii\web\View $this
 * @var \soc\yiiuser\User\Module $module
 */

$this->title = Yii::t('app', 'Rules');
$this->params['breadcrumbs'][] = $this->title;

?>

<?php $this->beginContent('@app/views/user/shared/admin_layout.php') ?>
<div class="table-responsive">
<?= GridView::widget(
    [
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'layout' => "{items}\n{pager}",
        'columns' => [
            [
                'attribute' => 'name',
                'label' => Yii::t('app', 'Name'),
                'options' => [
                    'style' => 'width: 20%'
                ],
            ],
            [
                'attribute' => 'className',
                'label' => Yii::t('app', 'Class'),
                'value' => function ($row) {
                    $rule = unserialize($row['data']);

                    return get_class($rule);
                },
                'options' => [
                    'style' => 'width: 20%'
                ],
            ],
            [
                'attribute' => 'created_at',
                'label' => Yii::t('app', 'Created at'),
                'format' => 'datetime',
                'options' => [
                    'style' => 'width: 20%'
                ],
            ],
            [
                'attribute' => 'updated_at',
                'label' => Yii::t('app', 'Updated at'),
                'format' => 'datetime',
                'options' => [
                    'style' => 'width: 20%'
                ],
            ],
            [
                'class' => ActionColumn::class,
                'template' => '{update} {delete}',
                'urlCreator' => function ($action, $model) {
                    return Url::to(['/user/rule/' . $action, 'name' => $model['name']]);
                },
                'options' => [
                    'style' => 'width: 5%'
                ],
            ]
        ],
    ]
) ?>
</div>

<?php $this->endContent() ?>
