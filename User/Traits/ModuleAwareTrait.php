<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Traits;

use soc\yiiuser\User\Module;
use Yii;

/**
 * @property-read Module $module
 */
trait ModuleAwareTrait
{
    /**
     * @return Module
     */
    public function getModule()
    {
        $user_module = Yii::$app->getModule('user');
        if (!$user_module){
          $user_module = new \stdClass();
          $user_module->minPasswordRequirements = null;
          $user_module->enableGdprCompliance = false;
          $user_module->blowfishCost = 10;
          $user_module->generatePasswords = false;
          $user_module->enableFlashMessages = false;
        }
        return $user_module;
    }
}
