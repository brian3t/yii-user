<?php

namespace soc\yiiuser\User\Traits;

use soc\yiiuser\User\Helper\AuthHelper;
use soc\yiiuser\User\Helper\ClassMapHelper;
use Yii;
use yii\base\InvalidConfigException;
use yii\di\Container;

/**
 * @property-read Container $di
 * @property-ready soc\yiiuser\User\Helper\AuthHelper $auth
 * @property-ready soc\yiiuser\User\Helper\ClassMapHelper $classMap
 */
trait ContainerAwareTrait
{
    /**
     * @return Container
     */
    public function getDi()
    {
        return Yii::$container;
    }

    /**
     * Gets a class from the container.
     *
     * @param string $class  he class name or an alias name (e.g. `foo`) that was previously registered via [[set()]]
     *                       or [[setSingleton()]]
     * @param array  $params constructor parameters
     * @param array  $config attributes
     *
     * @throws InvalidConfigException
     * @return object
     */
    public function make($class, $params = [], $config = [])
    {
      $di = $this->getDi();
      return $di->get($class, $params, $config);
    }

    /**
     * @throws InvalidConfigException
     * @return \soc\yiiuser\User\Helper\AuthHelper|object
     *
     */
    public function getAuth()
    {
        return $this->getDi()->get(AuthHelper::class);
    }

    /**
     * @throws InvalidConfigException
     * @return \soc\yiiuser\User\Helper\ClassMapHelper|object
     *
     */
    public function getClassMap()
    {
        return $this->getDi()->get(ClassMapHelper::class);
    }
}
