<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Strategy;

use soc\yiiuser\User\Contracts\MailChangeStrategyInterface;
use soc\yiiuser\User\Factory\MailFactory;
use soc\yiiuser\User\Factory\TokenFactory;
use soc\yiiuser\User\Form\SettingsForm;
use soc\yiiuser\User\Model\User;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use Yii;

class SecureEmailChangeStrategy implements MailChangeStrategyInterface
{
    use ContainerAwareTrait;

    protected $form;

    public function __construct(SettingsForm $form)
    {
        $this->form = $form;
    }

    public function run()
    {
        if ($this->make(DefaultEmailChangeStrategy::class, [$this->form])->run()) {
            $token = TokenFactory::makeConfirmOldMailToken($this->form->getUser()->id);
            $mailService = MailFactory::makeReconfirmationMailerService($this->form->getUser(), $token);

            if ($mailService->run()) {
                // unset flags if they exist
                $this->form->getUser()->flags &= ~User::NEW_EMAIL_CONFIRMED;
                $this->form->getUser()->flags &= ~User::OLD_EMAIL_CONFIRMED;
                if ($this->form->getUser()->save(false)) {
                    Yii::$app
                        ->session
                        ->setFlash(
                            'info',
                            Yii::t(
                                'app',
                                'We have sent confirmation links to both old and new email addresses. You must click both links to complete your request.'
                            )
                        );

                    return true;
                }
            }
        }

        return false;
    }
}
