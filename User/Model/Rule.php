<?php

namespace soc\yiiuser\User\Model;

use soc\yiiuser\User\Traits\AuthManagerAwareTrait;
use soc\yiiuser\User\Validator\RbacRuleNameValidator;
use soc\yiiuser\User\Validator\RbacRuleValidator;
use Yii;
use yii\base\Model;

class Rule extends Model
{
    use AuthManagerAwareTrait;

    /**
     * @var string
     */
    public $name;
    /**
     * @var string fully qualified class name. Not to be confused with className() method
     */
    public $className;
    /**
     * @var string holds the name of the rule previous update
     */
    public $previousName;

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'create' => ['name', 'className'],
            'update' => ['name', 'className', 'previousName'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'className'], 'trim'],
            [['name', 'className'], 'required'],
            [['name', 'previousName'], 'match', 'pattern' => '/^\w[\w.:\-]+\w$/'],
            [['name'], RbacRuleNameValidator::class, 'previousName' => $this->previousName],
            [['className'], RbacRuleValidator::class],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'name' => Yii::t('app', 'Name'),
            'className' => Yii::t('app', 'Rule class name'),
        ];
    }
}
