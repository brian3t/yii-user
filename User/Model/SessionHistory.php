<?php

namespace soc\yiiuser\User\Model;

use soc\yiiuser\User\Module;
use soc\yiiuser\User\Query\SessionHistoryQuery;
use soc\yiiuser\User\Traits\ModuleAwareTrait;
use Yii;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveQuery;
use yii\db\ActiveRecord;

/**
 * @property int    $user_id
 * @property string $session_id
 * @property string $user_agent
 * @property string $ip
 * @property int    $created_at
 * @property int    $updated_at
 *
 * @property User $user
 * @property bool $isActive
 *
 * Dependencies:
 * @property-read Module $module
 */
class SessionHistory extends ActiveRecord
{
    use ModuleAwareTrait;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%session_history}}';
    }

    /** @inheritdoc */
    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::class,
                'updatedAtAttribute' => false,
            ]
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('app', 'User ID'),
            'session_id' => Yii::t('app', 'Session ID'),
            'user_agent' => Yii::t('app', 'User agent'),
            'ip' => Yii::t('app', 'IP'),
            'created_at' => Yii::t('app', 'Created at'),
            'updated_at' => Yii::t('app', 'Last activity'),
        ];
    }

    /**
     * @return bool Whether the session is an active or not.
     */
    public function getIsActive()
    {
        return isset($this->session_id) && $this->updated_at + $this->getModule()->rememberLoginLifespan > time();
    }

    /**
     * @return ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne($this->module->classMap['User'], ['id' => 'user_id']);
    }

    /** @inheritdoc */
    public function beforeSave($insert)
    {
        if ($insert && empty($this->session_id)) {
            $this->setAttribute('session_id', Yii::$app->session->getId());
        }

        return parent::beforeSave($insert);
    }

    /** @inheritdoc */
    public static function primaryKey()
    {
        return ['user_id', 'session_id'];
    }

    public static function find()
    {
        return new SessionHistoryQuery(static::class);
    }
}
