<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Form;

use soc\yiiuser\User\Helper\SecurityHelper;
use soc\yiiuser\User\Model\User;
use soc\yiiuser\User\Query\UserQuery;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use soc\yiiuser\User\Traits\ModuleAwareTrait;
use Yii;
use yii\base\InvalidParamException;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

class LoginForm extends ActiveRecord
{
    use ModuleAwareTrait;
    use ContainerAwareTrait;

    /**
     * @var string login User's email or username
     */
    public $login;
    /**
     * @var string User's password
     */
    public $password;
    /**
     * @var string User's two-factor authentication code
     */
    public $twoFactorAuthenticationCode;
    /**
     * @var bool whether to remember User's login
     */
    public $rememberMe = false;
    /**
     * @var User
     */
    protected $user;
    /**
     * @var UserQuery
     */
    protected $query;
    /**
     * @var SecurityHelper
     */
    protected $securityHelper;

    public static function tableName(): string {
        return 'user';
    }

    /**
     * @param UserQuery      $query
     * @param SecurityHelper $securityHelper
     * @param array          $config
     */
    public function __construct(UserQuery $query = new UserQuery(LoginForm::class), SecurityHelper $securityHelper = new SecurityHelper(new \yii\base\Security()), $config = [])
    {
        $this->query = $query;
        $this->securityHelper = $securityHelper;
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'login' => Yii::t('app', 'Login'),
            'password' => Yii::t('app', 'Password'),
            'rememberMe' => Yii::t('app', 'Remember me next time'),
            'twoFactorAuthenticationCode' => Yii::t('app', 'Two factor authentication code')
        ];
    }

    /**
     * {@inheritdoc}
     *
     * @throws \soc\yiiuser\TwoFA\Exception\InvalidSecretKeyException (only if package is being used)
     */
    public function rules()
    {
        return [
            'requiredFields' => [['login', 'password'], 'required'],
            'requiredFieldsTwoFactor' => [
                ['login', 'password', 'twoFactorAuthenticationCode'],
                'required',
                'on' => '2fa'
            ],
            'loginTrim' => ['login', 'trim'],
            'twoFactorAuthenticationCodeTrim' => ['twoFactorAuthenticationCode', 'trim'],
            'passwordValidate' => [
                'password',
                function ($attribute) {
                    if ($this->user === null ||
                        !$this->securityHelper->validatePassword($this->password, $this->user->password_hash)
                    ) {
                        $this->addError($attribute, Yii::t('app', 'Invalid login or password'));
                    }
                },
            ],
            'twoFactorAuthenticationCodeValidate' => [
                'twoFactorAuthenticationCode',
                function ($attribute) {
                    if ($this->user === null) {
                        $this->addError($attribute, Yii::t('app', 'Invalid two factor authentication code'));
                    } else {
                        $module = Yii::$app->getModule('user');
                        $validators = $module->twoFactorAuthenticationValidators;
                        $type = $this->user->auth_tf_type;
                        $class = ArrayHelper::getValue($validators, $type.'.class');
                        $codeDurationTime = ArrayHelper::getValue($validators, $type.'.codeDurationTime', 300);
                        $validator = $this
                        ->make($class, [$this->user, $this->twoFactorAuthenticationCode, $this->module->twoFactorAuthenticationCycles]);
                        $success = $validator->validate();
                        if (!$success) {
                            $this->addError($attribute, $validator->getUnsuccessLoginMessage($codeDurationTime));
                        }
                    }
                }
            ],
            'confirmationValidate' => [
                'login',
                function ($attribute) {
                    if ($this->user !== null) {
                        $module = $this->getModule();
                        $confirmationRequired = $module->enableEmailConfirmation && !$module->allowUnconfirmedEmailLogin;
                        if ($confirmationRequired && !$this->user->getIsConfirmed()) {
                            $this->addError($attribute, Yii::t('app', 'You need to confirm your email address'));
                        }
                        if ($this->user->getIsBlocked()) {
                            $this->addError($attribute, Yii::t('app', 'Your account has been blocked'));
                        }
                    }
                },
            ],
            'rememberMe' => ['rememberMe', 'boolean'],
        ];
    }

    /**
     * Validates form and logs the user in.
     *
     * @throws InvalidParamException
     * @return bool                  whether the user is logged in successfully
     */
    public function login()
    {
        if ($this->validate()) {
            $duration = $this->rememberMe ? $this->module->rememberLoginLifespan : 0;

            return Yii::$app->getUser()->login($this->user, $duration);
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function beforeValidate()
    {
        if (parent::beforeValidate()) {
            $this->user = User::find()->whereUsernameOrEmail(trim($this->login))->one();

            return true;
        }

        return false;
    }

    /**
     * {@inheritdoc}
     */
    public function setUserAttribute()
    {
      $this->user = $this->query->whereUsernameOrEmail(trim($this->login))->one();
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
