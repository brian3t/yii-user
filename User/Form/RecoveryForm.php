<?php

namespace soc\yiiuser\User\Form;

use soc\yiiuser\User\Query\UserQuery;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use Yii;
use yii\base\Model;

class RecoveryForm extends Model
{
    use ContainerAwareTrait;

    const SCENARIO_REQUEST = 'request';
    const SCENARIO_RESET = 'reset';

    /**
     * @var string User's email
     */
    public $email;
    /**
     * @var string User's password
     */
    public $password;
    /**
     * @var UserQuery
     */
    protected $query;

    /**
     * @param UserQuery $query
     * @param array     $config
     */
    public function __construct(UserQuery $query, array $config = [])
    {
        $this->query = $query;
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'email' => Yii::t('app', 'Email'),
            'password' => Yii::t('app', 'Password'),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        return [
            self::SCENARIO_REQUEST => ['email'],
            self::SCENARIO_RESET => ['password'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'emailTrim' => ['email', 'filter', 'filter' => 'trim'],
            'emailRequired' => ['email', 'required'],
            'emailPattern' => ['email', 'email'],
            'passwordRequired' => ['password', 'required'],
            'passwordLength' => ['password', 'string', 'max' => 72, 'min' => 6],
        ];
    }
}
