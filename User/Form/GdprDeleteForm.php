<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

namespace soc\yiiuser\User\Form;

use soc\yiiuser\User\Helper\SecurityHelper;
use soc\yiiuser\User\Model\User;
use soc\yiiuser\User\Traits\ContainerAwareTrait;
use Yii;
use yii\base\Model;

/**
 * Class GdprDeleteForm
 * @package soc\yiiuser\User\Form
 */
class GdprDeleteForm extends Model
{
    use ContainerAwareTrait;

    /**
     * @var string User's password
     */
    public $password;
    /**
     * @var SecurityHelper
     */
    protected $securityHelper;
    /**
     * @var User
     */
    protected $user;

    /**
     * @param SecurityHelper $securityHelper
     * @param array          $config
     */
    public function __construct(SecurityHelper $securityHelper, $config = [])
    {
        $this->securityHelper = $securityHelper;
        parent::__construct($config);
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            'requiredFields' => [['password'], 'required'],
            'passwordValidate' => [
                'password',
                function ($attribute) {
                    if (!$this->securityHelper
                        ->validatePassword($this->password, $this->getUser()->password_hash)
                    ) {
                        $this->addError($attribute, Yii::t('app', 'Invalid password'));
                    }
                },
            ]
        ];
    }

    /**
     * @return User|null|\yii\web\IdentityInterface
     */
    public function getUser()
    {
        if ($this->user == null) {
            $this->user = Yii::$app->user->identity;
        }

        return $this->user;
    }

    public function attributeLabels()
    {
        return [
            'password' => Yii::t('app', 'Password'),
        ];
    }
}
